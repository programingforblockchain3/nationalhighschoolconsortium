// document.addEventListener('DOMContentLoaded', () => {
//   const issueForm = document.getElementById('issueForm');
//   const retrieveForm = document.getElementById('retrieveForm');
//   const certificateInfo = document.getElementById('certificateInfo');

//   issueForm.addEventListener('submit', async (event) => {
//     event.preventDefault();
//     const formData = new FormData(issueForm);
//     const requestData = {};

//     for (const [key, value] of formData.entries()) {
//       if (key === 'isInternational' || key === 'isPrivate') {
//         requestData[key] = value === 'on';
//       } else {
//         requestData[key] = value;
//       }
//     }

//     try {
//       const response = await fetch('/certificates', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify(requestData),
//       });

//       if (response.ok) {
//         alert('Certificate issued successfully!');
//       } else {
//         const errorMessage = await response.text();
//         alert(`Failed to issue certificate: ${errorMessage}`);
//       }
//     } catch (error) {
//       console.error('Error issuing certificate:', error);
//       alert('Error issuing certificate. Please try again.');
//     }
//   });

//   retrieveForm.addEventListener('submit', async (event) => {
//       event.preventDefault();

//       const certificateId = document.getElementById('certificateId').value;

//       try {
//           const response = await fetch(`/certificates/${certificateId}`);

//           if (response.ok) {
//               const certificate = await response.json();
//               certificateInfo.innerHTML = `<pre>${JSON.stringify(certificate, null, 2)}</pre>`;
//           } else {
//               certificateInfo.innerHTML = 'Certificate not found';
//           }
//       } catch (error) {
//           console.error('Error retrieving certificate:', error);
//           certificateInfo.innerHTML = 'Error retrieving certificate. Please try again.';
//       }
//   });
// });


// $(document).ready(function () {
//   $('#confirmModal').on('hidden.bs.modal', function () {
//     $('#yesButton').off("click");
//   });

//   $('#noButton').click(function () {
//     $('#confirmModal').modal('hide');
//   });

//   $('#createCertificateForm').on('submit', function (e) {
//     e.preventDefault();
//     $('#yesButton').off("click").click(IssueCertificate);
//     $('#confirmModal').modal('show');
//   });

//   $('#getCertificateForm').on('submit', function (e) {
//     e.preventDefault();
//     var searchId = $('#searchId').val();
//     $.ajax({
//       url: '/certificates/' + searchId,
//       type: 'GET',
//       success: function (data) {
//         $('#editCertificateId').text(data.id);
//         $('#editStudentName').val(data.studentName);
//         $('#editCertificateName').val(data.certificateName);
//         $('#editSchool').val(data.school);
//         $('#editIssuedDate').val(data.issuedDate.split('T')[0]);
//         $('#editIssuedBy').val(data.issuedBy);
//         $('#editIsInternational').prop('checked', data.isInternational);
//         $('#editIsPrivate').prop('checked', data.isPrivate);
//         $('#editCertificateForm').removeClass('d-none');
//       },
//       error: function (error) {
//         $('#message').text("Certificate not found").addClass('text-danger');
//       }
//     });
//   });

//   $('#updateButton').click(function () {
//     $('#yesButton').off("click").click(updateCertificate);
//     $('#confirmModal').modal('show');
//   });

//   $('#deleteButton').click(function () {
//     $('#yesButton').off("click").click(deleteCertificate);
//     $('#confirmModal').modal('show');
//   });
// });

// function IssueCertificate() {
//   var data = {
//     id: $('#certificateId').val(),
//     studentName: $('#studentName').val(),
//     certificateName: $('#certificateName').val(),
//     school: $('#school').val(),
//     issuedDate: new Date($('#issuedDate').val()).toISOString(), // Convert date to the expected format
//     issuedBy: $('#issuedBy').val(),
//     isInternational: $('#isInternational').is(':checked'),
//     isPrivate: $('#isPrivate').is(':checked')
//   };

//   $.ajax({
//     url: '/certificates',
//     type: 'POST',
//     data: JSON.stringify(data),
//     contentType: 'application/json; charset=utf-8',
//     success: function () {
//       $('#successModal').modal('show');
//     },
//     error: function (error) {
//       console.log(error);
//       $('#successModal').modal('hide');
//     }
//   }).always(function () {
//     $('form')[0].reset();
//     $('#confirmModal').modal('hide');
//   });
// }

// function updateCertificate() {
//   var data = {
//     studentName: $('#editStudentName').val(),
//     certificateName: $('#editCertificateName').val(),
//     school: $('#editSchool').val(),
//     issuedDate: $('#editIssuedDate').val(),
//     issuedBy: $('#editIssuedBy').val(),
//     isInternational: $('#editIsInternational').is(':checked'),
//     isPrivate: $('#editIsPrivate').is(':checked')
//   };

//   var certificateId = $('#editCertificateId').text();

//   $.ajax({
//     url: '/certificates/' + certificateId,
//     type: 'PUT',
//     data: JSON.stringify(data),
//     contentType: 'application/json; charset=utf-8',
//     success: function () {
//       var myModal = new bootstrap.Modal($('#successModal'), {});
//       myModal.show();
//     },
//     error: function (error) {
//       console.log(error);
//     }
//   }).always(function () {
//     $('#confirmModal').modal('hide');
//   });
// }

// function deleteCertificate() {
//   var certificateId = $('#editCertificateId').text();
//   $.ajax({
//     url: '/certificates/' + certificateId,
//     type: 'DELETE',
//     success: function () {
//       var myModal = new bootstrap.Modal($('#successModal'), {});
//       myModal.show();
//     },
//     error: function (error) {
//       console.log(error);
//     }
//   }).always(function () {
//     $('#editCertificateForm').addClass('d-none');
//     $('#confirmModal').modal('hide');
//   });
// }


$(document).ready(function () {
    $('#confirmModal').on('hidden.bs.modal', function () {
        $('#yesButton').off("click");
    });

    $('#noButton').click(function () {
        $('#confirmModal').modal('hide');
    });

    $('#createCertificateForm').on('submit', function (e) {
        e.preventDefault();
        $('#yesButton').off("click").click(IssueCertificate);
        $('#confirmModal').modal('show');
    });

    $('#getCertificateForm').on('submit', function (e) {
        e.preventDefault();
        var searchId = $('#searchId').val();
        $.ajax({
            url: '/certificates/' + searchId,
            type: 'GET',
            success: function (data) {
                console.log("dataasmdbask", data);
                data = JSON.parse(data);
                $('#editCertificateId').text(data.id);
                $('#editStudentName').val(data.studentName);
                $('#editCertificateName').val(data.certificateName);
                $('#editSchool').val(data.school);

                var formattedIssuedDate = formattedDate(data.issuedDate);
                $('#editIssuedDate').val(formattedIssuedDate);
                $('#editIssuedBy').val(data.issuedBy);
                $('#editIsInternational').prop('checked', data.isInternational);
                $('#editIsPrivate').prop('checked', data.isPrivate);
                $('#editCertificateForm').removeClass('d-none');
            },
            error: function (error) {
                console.error(error);
                $('#message').text("Certificate not found since it has been Deleted!").addClass('text-danger');
            }
        });
    });

    $('#updateButton').click(function () {
        $('#yesButton').off("click").click(updateCertificate);
        $('#confirmModal').modal('show');
    });

    $('#deleteButton').click(function () {
        $('#yesButton').off("click").click(deleteCertificate);
        $('#confirmModal').modal('show');
    });
});

function formattedDate(rfc3339Date) {
    if (!rfc3339Date) {
        return '';
    }
    var date = new Date(rfc3339Date);
    if (isNaN(date.getTime())) {
        console.error("Invalid date format");
        return '';
    }
    var day = date.getDate(); // day of the month
    var month = date.getMonth() + 1; // month (getMonth() returns 0-11, so add 1)
    month = month < 10 ? "0" + month : month;
    day = day < 10 ? "0" + day : day;
    var year = date.getFullYear(); // year
    // format date as YYYY-MM-DD
    var formattedDate = year + '-' + month + '-' + day;
    return formattedDate;
}

function IssueCertificate() {
    var data = {
        id: $('#certificateId').val(),
        studentName: $('#studentName').val(),
        certificateName: $('#certificateName').val(),
        school: $('#school').val(),
        issuedDate: new Date($('#issuedDate').val()).toISOString(), // Convert date to the expected format
        issuedBy: $('#issuedBy').val(),
        isInternational: $('#isInternational').is(':checked'),
        isPrivate: $('#isPrivate').is(':checked')
    };

    $.ajax({
        url: '/certificates',
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        success: function () {
            $('#successModal').modal('show');
        },
        error: function (error) {
            console.log(error);
            $('#successModal').modal('hide');
        }
    }).always(function () {
        $('form')[0].reset();
        $('#confirmModal').modal('hide');
    });
}

// function updateCertificate() {
//   var data = {
//       studentName: $('#editStudentName').val(),
//       certificateName: $('#editCertificateName').val(),
//       school: $('#editSchool').val(),
//       issuedDate: $('#editIssuedDate').val(),
//       issuedBy: $('#editIssuedBy').val(),
//       isInternational: $('#editIsInternational').is(':checked'),
//       isPrivate: $('#editIsPrivate').is(':checked')
//   };

//   var certificateId = $('#editCertificateId').text();

//   $.ajax({
//       url: '/certificates/' + certificateId,
//       type: 'PUT',
//       data: JSON.stringify(data),
//       contentType: 'application/json; charset=utf-8',
//       success: function () {
//           var myModal = new bootstrap.Modal($('#successModal'), {});
//           myModal.show();
//       },
//       error: function (error) {
//           console.log(error);
//       }
//   }).always(function () {
//       $('#confirmModal').modal('hide');
//   });
// }

function updateCertificate() {
    // Collect updated certificate data from the form
    var certificateData = {
        id: $('#editCertificateId').text().trim(),
        studentName: $('#editStudentName').val(),
        certificateName: $('#editCertificateName').val(),
        school: $('#editSchool').val(),
        issuedDate: $('#editIssuedDate').val(), // Assuming the date is in ISO format
        issuedBy: $('#editIssuedBy').val(),
        isInternational: $('#editIsInternational').is(':checked'),
        isPrivate: $('#editIsPrivate').is(':checked'),
        NationalSchool: $('#editNationalSchool').is(':checked')
    };

    // Send an AJAX request to update the certificate
    $.ajax({
        url: '/certificates/' + certificateData.id, // Assuming the endpoint for updating certificates is '/certificates/:id'
        type: 'PUT', // Use the PUT method to update data
        contentType: 'application/json', // Set the content type to JSON
        data: JSON.stringify(certificateData), // Convert the certificate data to a JSON string
        success: function (response) {
            // Handle success response
            console.log("Certificate updated successfully:", response);
            // Optionally, you can display a success message or perform any other actions here
        },
        error: function (xhr, status, error) {
            // Handle error response
            console.error("Failed to update certificate:", error);
            // Optionally, you can display an error message or perform any other error handling here
        }
    });
}


function deleteCertificate() {
    var certificateId = $('#editCertificateId').text();
    $.ajax({
        url: '/certificates/' + certificateId,
        type: 'DELETE',
        success: function () {
            // Show delete success message
            $('#deleteSuccessMessage').removeClass('d-none');
            // Hide the message after a certain time (optional)
            setTimeout(function () {
                $('#deleteSuccessMessage').addClass('d-none');
            }, 3000); // 3000 milliseconds = 3 seconds

            // Optionally, you can also hide the edit form or perform other actions here
        },
        error: function (error) {
            console.log(error);
        }
    }).always(function () {
        $('#editCertificateForm').addClass('d-none');
        $('#confirmModal').modal('hide');
    });
}

