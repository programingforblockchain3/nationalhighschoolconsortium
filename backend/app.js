const express = require('express');
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(cors()); // Enable CORS for all origins

// Static Middleware
app.use(express.static(path.join(__dirname, 'public')));

// Create certificate
app.post('/certificates', async (req, res) => {
    try {
      const { id, studentName, certificateName, school, issuedDate, issuedBy } = req.body;
      const isInternational = req.body.isInternational === undefined ? false : req.body.isInternational;
      const isPrivate = req.body.isPrivate === undefined ? false : req.body.isPrivate;
      const result = await submitTransaction('IssueCertificate', id, studentName, certificateName, school, issuedDate, issuedBy, isInternational, isPrivate);
      res.status(201).json(result);
    } catch (error) {
      console.error(`Failed to submit transaction: ${error}`);
      res.status(500).send(`Failed to submit transaction: ${error}`);
    }
  });

// Read certificate by ID
app.get('/certificates/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const result = await evaluateTransaction('ReadCertificate', id);
    res.status(200).json(result);
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    res.status(404).send(`Failed to evaluate transaction: ${error}`);
  }
});

// // Update certificate
// app.put('/certificates/:id', async (req, res) => {
//   try {
//     const { id } = req.params;
//     const { studentName, certificateName, school, issuedDate, issuedBy, isInternational, isPrivate } = req.body;
//     const result = await submitTransaction('updateCertificate', id, studentName, certificateName, school, issuedDate, issuedBy, isInternational, isPrivate);
//     res.status(200).json(result);
//   } catch (error) {
//     console.error(`Failed to submit transaction: ${error}`);
//     res.status(500).send(`Failed to submit transaction: ${error}`);
//   }
// });
// Update certificate
// Update certificate
app.put('/certificates/:id', async (req, res) => {
  try {
    const certificateData = req.body; // Get the entire certificate object from the request body

    // Parse JSON string into a JavaScript object
    const certificate = {
        ID: certificateData.id,
        StudentName: certificateData.studentName,
        CertificateName: certificateData.certificateName,
        School: certificateData.school,
        IssuedDate: new Date(certificateData.issuedDate),
        IssuedBy: certificateData.issuedBy,
        IsInternational: certificateData.isInternational,
        IsPrivate: certificateData.isPrivate,
        NationalSchool: certificateData.NationalSchool
    };

    // Pass the entire certificate object as a single argument
    const result = await submitTransaction('updateCertificate', certificate);
    
    res.status(200).json(result);
  } catch (error) {
    console.error(`Failed to submit transaction: ${error}`);
    res.status(500).send(`Failed to submit transaction: ${error}`);
  }
});



// Delete certificate
app.delete('/certificates/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const result = await submitTransaction('DeleteCertificate', id);
    res.status(204).send();
  } catch (error) {
    console.error(`Failed to submit transaction: ${error}`);
    res.status(500).send(`Failed to submit transaction: ${error}`);
  }
});

async function getContract() {
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const identity = await wallet.get('Admin@nationalschool.edu');
    const gateway = new Gateway();
    const connectionProfile = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'connection.json'), 'utf8'));
    const connectionOptions = {
      wallet,
      identity: identity,
      discovery: { enabled: false, asLocalhost: true }
    };
    await gateway.connect(connectionProfile, connectionOptions);
    const network = await gateway.getNetwork('nationalschoolchannel');
    const contract = network.getContract('certificatemgt');
    return contract;
  }

async function submitTransaction(functionName, ...args) {
  const contract = await getContract();
  console.log(contract)
  const result = await contract.submitTransaction(functionName, ...args);
  console.log("result", result.toString());
  return result.toString();
}

async function evaluateTransaction(functionName, ...args) {
  const contract = await getContract();
  const result = await contract.evaluateTransaction(functionName, ...args);
  return result.toString();
}

app.get('/', (req, res) => {
  res.send('Hello, World!');
});
module.exports=app;