package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type Certificate struct {
	ID              string    `json:"id"`
	StudentName     string    `json:"studentName"`
	CertificateName string    `json:"certificateName"`
	School          string    `json:"school"`
	IssuedDate      time.Time `json:"issuedDate"`
	IssuedBy        string    `json:"issuedBy"`
	IsInternational bool      `json:"isInternational"`
	IsPrivate       bool      `json:"isPrivate"`
	NationalSchool  bool	  `json:"NationalSchool"`
}

type CertificateContract struct {
	contractapi.Contract
}


func (c *CertificateContract) ApproveInternationalSchool(ctx contractapi.TransactionContextInterface, id string) error {
    certificate, err := c.ReadCertificate(ctx, id)
    if err != nil {
        return err
    }

    certificate.IsInternational = true

    return c.updateCertificate(ctx, certificate)
}

func (c *CertificateContract) ApprovePrivateSchool(ctx contractapi.TransactionContextInterface, id string) error {
    certificate, err := c.ReadCertificate(ctx, id)
    if err != nil {
        return err
    }

    certificate.IsPrivate = true
    return c.updateCertificate(ctx, certificate)
}

func (c *CertificateContract) ApproveNationalSchool(ctx contractapi.TransactionContextInterface, id string, nationalschool string) error {
    if nationalschool != "hns" && nationalschool != "nms" && nationalschool != "nps" {
        return fmt.Errorf("school can only be approved by hns, nps, or nms")
    }

    certificate, err := c.ReadCertificate(ctx, id)
    if err != nil {
        return err
    }

    certificate.NationalSchool = true // Set NationalSchool based on nationalschool parameter
    return c.updateCertificate(ctx, certificate)
}

func (c *CertificateContract) IssueCertificate(ctx contractapi.TransactionContextInterface, id string, studentName string, certificateName string, school string, issuedDate time.Time, issuedBy string, isInternational bool, isPrivate bool) error {
	if len(id) == 0 || len(studentName) == 0 || len(certificateName) == 0 || len(school) == 0 || len(issuedBy) == 0 {
		return fmt.Errorf("all input fields must be non-empty")
	}

	certificate := Certificate{
		ID:              id,
		StudentName:     studentName,
		CertificateName: certificateName,
		School:          school,
		IssuedDate:      issuedDate,
		IssuedBy:        issuedBy,
		IsInternational: isInternational,
		IsPrivate:       isPrivate,
	}

	certificateJSON, err := json.Marshal(certificate)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(id, certificateJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}

	return nil
}

func (c *CertificateContract) ReadCertificate(ctx contractapi.TransactionContextInterface, id string) (*Certificate, error) {
	certificateJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}

	if certificateJSON == nil {
		return nil, fmt.Errorf("the certificate %s does not exist", id)
	}

	var certificate Certificate
	err = json.Unmarshal(certificateJSON, &certificate)
	if err != nil {
		return nil, err
	}

	return &certificate, nil
}

func (c *CertificateContract) updateCertificate(ctx contractapi.TransactionContextInterface, certificate *Certificate) error {
    certificateJSON, err := json.Marshal(certificate)
    if err != nil {
        return err
    }

    err = ctx.GetStub().PutState(certificate.ID, certificateJSON)
    if err != nil {
        return fmt.Errorf("failed to update certificate in world state: %v", err)
    }

    return nil
}

func (c *CertificateContract) DeleteCertificate(ctx contractapi.TransactionContextInterface, id string) error {
	err := ctx.GetStub().DelState(id)
	if err != nil {
		return fmt.Errorf("failed to delete certificate from world state: %v", err)
	}

	return nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(CertificateContract))
	if err != nil {
		fmt.Printf("Error creating certificate chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting certificate chaincode: %s", err.Error())
	}
}
