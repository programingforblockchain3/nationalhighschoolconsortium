#!/bin/bash
export ORG_CONTEXT=privateschool
export ORG_NAME=Privateschool 
export CORE_PEER_LOCALMSPID=PrivateschoolMSP
# Logging specifications
export FABRIC_LOGGING_SPEC=INFO
# Location of the core.yaml
export FABRIC_CFG_PATH=/workspaces/nationalhighschoolconsortium/config/pri
# Address of the peer
export CORE_PEER_ADDRESS=pri.privateschool.edu:7051
# Local MSP for the admin - Commands need to be executed as org admin
export CORE_PEER_MSPCONFIGPATH=/workspaces/nationalhighschoolconsortium/config/crypto-config/peerOrganizations/privateschool.edu/users/Admin@privateschool.edu/msp 
# Address of the orderer
export ORDERER_ADDRESS=orderer.nationalschool.edu:7050
export CORE_PEER_TLS_ENABLED=false
#### Chaincode related properties
export CC_NAME="certificatemgt"
export CC_PATH="./chaincodes/certificatemgt/"
export CC_CHANNEL_ID="nationalschoolchannel"
export CC_LANGUAGE="golang"
# Properties of Chaincode
export INTERNAL_DEV_VERSION="1.0"
export CC_VERSION="1.0"
export CC2_PACKAGE_FOLDER="./chaincodes/packages/"
export CC2_SEQUENCE=1
export CC2_INIT_REQUIRED="--init-required"
# Create the package with this name
export CC_PACKAGE_FILE="$CC2_PACKAGE_FOLDER$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION.tar.gz"
# Extracts the package ID for the installed chaincode
export CC_LABEL="$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION"
peer channel list


