<!-- National School Consortium Hyperledger Fabric Network -->

This repository contains the code and configuration files for setting up a Hyperledger Fabric network for the National School Consortium. The network is designed to manage and issue digital certificates for schools and students.

<!-- Prerequisites -->

Docker
Docker Compose
Go programming language
Node.js and NPM

<!-- Network Setup -->

1.Generate the Orderer Genesis Block
#configtxgen -outputBlock ./orderer/nationalschoolgenesis.block -channelID ordererchannel -profile NationalschoolOrdererGenesis

2.Create the Channel Transaction
#configtxgen -outputCreateChannelTx ./nationalschoolchannel/nationalschoolchannel.tx -channelID nationalschoolchannel -profile NationalschoolChannel

3.Set the Peer Environment
#. tool-bins/set_peer_env.sh nationalschool

4.Create the Channel
#peer channel create -c nationalschoolchannel -f ./config/nationalschoolchannel/nationalschoolchannel.tx --outputBlock ./config/nationalschoolchannel/nationalschoolchannel.block -o $ORDERER_ADDRESS

5.Join the Peer to the Channel
#peer channel join -b ./config/nationalschoolchannel/nationalschoolchannel.block -o $ORDERER_ADDRESS

6.List the Channels
#peer channel list



<!-- Chaincode Operations -->

1.Approve the Chaincode for the Organization
#peer lifecycle chaincode approveformyorg -n certificatemgt -v 1.0 -C nationalschoolchannel --sequence 1 --package-id $CC_PACKAGE_ID

2.Check Commit Readiness
#peer lifecycle chaincode checkcommitreadiness -n certificatemgt -v 1.0 -C nationalschoolchannel --sequence 1

3.Commit the Chaincode
#peer lifecycle chaincode commit -n certificatemgt -v 1.0 -C nationalschoolchannel --sequence 1

4.Query Committed Chaincodes
#peer lifecycle chaincode querycommitted -n certificatemgt -C nationalschoolchannel

5.Invoke the Chaincode (Issue Certificate)
#peer chaincode invoke -C nationalschoolchannel -n certificatemgt -c '{"function":"IssueCertificate","Args":["1", "Sonam Tshering","ForParticipatance","Punakha School", "2000-01-01T00:00:00Z", "School Management","false","false"]}'

6.Query the Chaincode (Read Certificate)
#peer chaincode query -C nationalschoolchannel -n certificatemgt -c '{"function":"ReadCertificate","Args":["1"]}'

7.Invoke the Chaincode (Delete Certificate)
#peer chaincode invoke -C nationalschoolchannel -n certificatemgt -c '{"function":"DeleteCertificate","Args":["1"]}'


lease note that you may need to replace certain placeholders (e.g., $ORDERER_ADDRESS, $CC_PACKAGE_ID) with the appropriate values for your environment and need to package and install it before chaincode operation




<!-- Remember! Set the PATH for Hyperledger Fabric Binaries whenwver you close of rebuild conatiner -->
if [ -d "/workspaces/nationalhighschoolconsortium/bin" ] ; then PATH="/workspaces/nationalhighschoolconsortium/bin:$PATH" fi (your own path)
